-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2023-04-25 15:36:43
-- サーバのバージョン： 10.4.27-MariaDB
-- PHP のバージョン: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `devcamp_customer`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `customersql`
--

CREATE TABLE `customersql` (
  `id` int(80) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `PostalCode` int(80) NOT NULL,
  `Country` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- テーブルのデータのダンプ `customersql`
--

INSERT INTO `customersql` (`id`, `customer_name`, `contact_name`, `address`, `city`, `PostalCode`, `Country`) VALUES
(1, 'Alfreds Futterkiste', 'Maria Anders', 'Obere Str. 57', 'Berlin', 12209, 'Germany'),
(2, 'Ana Trujillo Emparedados y helados', 'Ana Trujillo', 'Avda. de la Constitución 2222', 'México D.F.', 5021, 'Mexico'),
(3, 'Antonio Moreno Taquería', 'Antonio Moreno', 'Mataderos 2312', 'México D.F.', 5023, 'Mexico'),
(4, 'Around the Horn', 'Thomas Hardy', '120 Hanover Sq.', 'London', 0, 'UK'),
(5, 'Berglunds snabbköp', 'Christina Berglund', 'Berguvsvägen 8', 'Luleå', 0, 'Sweden'),
(6, 'Hoang Van Manh', 'Bui Thi Xuan', '21 Luong Van Can', 'Ha noi', 100, 'Vietnam'),
(7, 'Nguyen Van Tam', 'Nguyen Tuong', '78 Tran Huung Dao', 'Ha noi', 100, 'Vietnam');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `customersql`
--
ALTER TABLE `customersql`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


_____________________________

SELECT Country, COUNT(*)  FROM customersql GROUP BY Country
SELECT city, COUNT(*)  FROM customersql GROUP BY city
SELECT Country, COUNT(*)  FROM customersql GROUP BY Country ORDER BY 2
SELECT city, COUNT(*)  FROM customersql GROUP BY city ORDER BY 2
